fx_version 'cerulean'
game 'gta5'
lua54 'yes'

description 'Skyes Helper Functions Lib'
author "SkyeBreach"
version '1.0.0'

dependencies {}

client_script {
  'client/cl_main.lua',
  'client/cl_debug.lua',
}

server_script {
  '@oxmysql/lib/MySQL.lua',
  'server/sv_main.lua',
  'server/sv_debug.lua'
}

shared_scripts {
	"shared/**/*"
}

escrow_ignore {
	"config.lua",
  "shared/**/*",
  "server/**/*",
  "client/**/*",
}