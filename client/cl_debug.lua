function SHF.Client.Debug.Functions.nicePrint(arr, indentLevel)
    if (type(arr) == 'string') then
        print(arr)
        return
    end
    if (type(arr) == 'vector3') then
        print("x: " .. arr.x .. ", y: " .. arr.y .. ", z: " .. arr.z )
        return
    end
    if (type(arr) == 'number') then
        print(arr)
        return
    end
    if (arr == nil) then
        print(arr)
        return
    end
    local str = ""
    local indentStr = "#"

    if(indentLevel == nil) then
        print(SHF.Client.Debug.Functions.nicePrint(arr, 0))
        return
    end

    for i = 0, indentLevel do
        indentStr = indentStr.."\t"
    end

    for index, value in pairs(arr) do
        if type(value) == "table" then
            str = str..indentStr..index..": \n"..SHF.Client.Debug.Functions.nicePrint(value, (indentLevel + 1))
        elseif type(value) == "boolean" then
            if value then
                str = str..indentStr..index..": True \n"
            else
                str = str..indentStr..index..": false \n"
            end
        else
            str = str..indentStr..index..": "..value.."\n"
        end
    end
    return str
end