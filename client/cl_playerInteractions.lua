function SHF.Client.Functions.raycast(length, bDebug)
    local ped = PlayerPedId()
    local vectorStart = GetGameplayCamCoord()--GetOffsetFromEntityInWorldCoords(GetCurrentPedWeaponEntityIndex(ped),0,0,0.01)
    local camDirection = GetGameplayCamRot()

    camDirection = SHF.Client.Functions.camDirToCoord(camDirection)

    local vectorDestination = vec3( -- Finds the end point of the vector
        vectorStart.x + (camDirection.x * length),
        vectorStart.y + (camDirection.y * length),
        vectorStart.z + (camDirection.z * length)
    )

    local rayHandle, result, hit, endCoords, surfaceNormal , entityHit = StartShapeTestLosProbe(vectorStart.x, vectorStart.y, vectorStart.z, vectorDestination.x, vectorDestination.y, vectorDestination.z, -1, ped, 1)
    repeat
        result, hit, endCoords, surfaceNormal , entityHit = GetShapeTestResult(rayHandle)
        Wait(0)
    until result ~= 1

    if (bDebug) then
        if (result == 1 ) then
            DrawLine(vectorStart.x, vectorStart.y, vectorStart.z, vectorDestination.x, vectorDestination.y, vectorDestination.z, 255, 0, 0, 255)
        else
            DrawLine(vectorStart.x, vectorStart.y, vectorStart.z, endCoords.x, endCoords.y, endCoords.z, 0, 255, 0, 255)
            print("Hit: " .. hit)
            print("endCoords: " .. endCoords)
            print("surfaceNormal: " .. surfaceNormal)
            print("entityHit: " .. entityHit)
            print("entityType: " .. GetEntityType(entityHit))
        end
    end

    return hit, endCoords, surfaceNormal, entityHit, GetEntityType(entityHit)


end

function SHF.Client.Functions.quickForce(entity, type,direction, rotation, isRelDir)
    local boneIndex = 0
    local isDirectionRel = false
    local ignoreUpVec = false
    local isForceRel = true
    local p12 = false
    local p13 = true
    ApplyForceToEntity(
        entity,
        type,
        direction,
        rotation ,
        boneIndex,
        isRelDir,
        ignoreUpVec,
        isForceRel,
        p12,
        p13
    )
end
