function SHF.Client.Functions.camDirToCoord(direction)
    local camDirection = vec3( --Conversion to radians
    (math.pi / 180) * direction.x,
    (math.pi / 180) * direction.y,
    (math.pi / 180) * direction.z
    )

    camDirection = vec3( -- converts the direction
        -math.sin(camDirection.z) * math.abs(math.cos(camDirection.x)),
        math.cos(camDirection.z) * math.abs(math.cos(camDirection.x)),
        math.sin(camDirection.x)
    )
    return camDirection
end
