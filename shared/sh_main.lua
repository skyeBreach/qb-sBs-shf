SHF = {}
SHF.Shared = {}
SHF.Shared.Functions = {}
SHF.Shared.Debug = {}
SHF.Shared.Debug.Functions = {}

exports('GetSharedObject', function()
    return SHF.Shared
end)



function SHF.Shared.Debug.Functions.NicePrint(arr, indentLevel)
    if (type(arr) == 'string') then
        print(arr)
        return
    end
    if (type(arr) == 'vector3') then
        print("x: " .. arr.x .. ", y: " .. arr.y .. ", z: " .. arr.z )
        return
    end
    if (type(arr) == 'number') then
        print(arr)
        return
    end
    if (arr == nil) then
        print(arr)
        return
    end
    local str = ""
    local indentStr = "#"

    if(indentLevel == nil) then
        print(SHF.Shared.Debug.Functions.NicePrint(arr, 0))
        return
    end

    for i = 0, indentLevel do
        indentStr = indentStr.."\t"
    end

    for index, value in pairs(arr) do
        if type(value) == "table" then
            str = str..indentStr..index..": \n"..SHF.Shared.Debug.Functions.NicePrint(value, (indentLevel + 1))
        elseif type(value) == "boolean" then
            if value then
                str = str..indentStr..index..": True \n"
            else
                str = str..indentStr..index..": false \n"
            end
        else
            str = str..indentStr..index..": "..value.."\n"
        end
    end
    return str
end

-- Dont touch this
function SHF.Shared.Functions.UpdateTable(bClearConfig, ConfigOrigional, ConfigUpdates)
    --eSHF.Shared.Debug.Functions.NicePrint(ConfigOrigional)
    if ConfigOrigional == nil then
        --print("ConfigOrigional == nil")
        ConfigOrigional = {}
    end
    if ConfigUpdates == nil then
        --print("ConfigUpdates == nil")
        return
    end
    if bClearConfig then ConfigOrigional = {} end

    for k, v in pairs(ConfigUpdates) do
        ConfigOrigional[k] = v
    end

    --SHF.Shared.Debug.Functions.NicePrint(ConfigOrigional)
    return ConfigOrigional
    --SHF.Shared.Debug.Functions.NicePrint(ConfigOrigional)
end